let mongoose = require('mongoose');
let Schema = mongoose.Schema;

// Create schema
let newsModel = new Schema({
    title: String,
    content: String,
    author: String,
});

module.exports = mongoose.model('news', newsModel);