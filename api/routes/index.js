let express = require('express');
let News = require('../models/News');
let router = express.Router();

// Get articles
router.get('/news', function(req, res, next) {
    News.find().then(function(result) {
        res.json({results: result});
    }).catch(function(error) {
        res.json({error});
    });
});

module.exports = router;
